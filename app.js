
/**
 * Module dependencies.
 */

var express = require('express')
  , routes = require('./routes')
  , user = require('./routes/user')
  , https = require('https')
  , path = require('path')
  , Facebook = require('facebook-node-sdk')
  , fs = require ('fs');

var expressLayouts = require('express-ejs-layouts');  
var app = express();

app.configure(function(){
  app.set('port', process.env.PORT || 3000);
  app.set('views', __dirname + '/views');
  app.set('view engine', 'ejs');
  app.use(express.favicon(__dirname + '/public/images/favicon.ico'));
  app.use(express.logger('dev'));
  app.use(express.bodyParser());
  app.use(express.methodOverride());
  app.use(express.cookieParser('your secret here'));
  app.use(express.session());
    app.use(expressLayouts);
  app.use(app.router);
  app.use(express.static(path.join(__dirname, 'public')));
  app.use(Facebook.middleware({ appId: '144183422433747', secret: '113d391fe8fcf03271afb9dae621514e' }));
});

app.configure('development', function(){
  app.use(express.errorHandler());
});

app.get('/', routes.index);
app.get('/users', user.list);
app.get('/torture', routes.torture)
app.get('/dead', routes.dead)
app.get('/survivor', routes.survivor)
app.post('/redtorture', routes.redtorture);
app.post('/guessing', routes.guessing);

var options = {
  key: fs.readFileSync('evilhangman.pem'),
  cert: fs.readFileSync('evilhangman-cert.pem')
};

https.createServer(options, app).listen(app.get('port'), function(){
    console.log('Connection established on port ' + app.get('port'));
    setTimeout(function() {
        console.log('Hello Ariel.');
        setTimeout(function() {
            console.log('I wanna play a game');
            setTimeout(function() {
                    console.log('For years you have tortured many students');
                    setTimeout(function() {
                        console.log('You\'ll have a chance to redeem yourself, for the games you\'ve played with others, by playing one of mine.');
                        setTimeout(function() {
                            console.log('Open your browser and go to https://localhost:' + app.get('port') + ' and you\'ll find my game.');
                            setTimeout(function() {
                                console.log('You\'ll have to choose wisely, otherwise you will pay with your life');
                                setTimeout(function() {
                                    console.log('If you win this game you\'ll keep your life... For now');
                                    setTimeout(function() {
                                        console.log('Let the game begin!');
                                        setTimeout(function() {
                                            console.log('Connection closed by foreign host. Go to https://localhost:' + app.get('port'));
                                        }, 2000);
                                    }, 2000);
                                }, 2000);
                            }, 2000);
                        }, 2000);
                    }, 2000);
            }, 2000);
        }, 2000);                
    }, 2000);
});

